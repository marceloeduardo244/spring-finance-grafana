FROM grafana/grafana:latest

COPY ./grafana /var/lib/grafana

CMD ["grafana-server", "--config=/etc/grafana/grafana.ini", "--homepath=/usr/share/grafana"]